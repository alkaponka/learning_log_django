                                                    learning_log

    "We will create a web application called Learning Log, through which the user will be able to keep a journal
    of topics that interest them and create entries in the journal while studying each topic. The Learning Log homepage
    contains a description of the site and invites the user to register or enter their credentials. After successful
    login, the user gains the ability to create new topics, add new entries, read, and edit existing entries."
    All the commands provided can be executed on Linux Ubuntu in the terminal.
    Local deployment:
    
        git clone https://gitlab.com/alkaponka/learning_log_django.git - To clone this repository
        python3 -m venv venv - Set up a virtual environment
        source venv/bin/activate - Activate the virtual environment
        pip install -r requirements.txt - Install dependencies from the file
        python manage.py runserver - Run the local server from the terminal with the virtual environment activated

    Remote deployment on Heroku:
        After you have everything working locally!
        Registration on: https://heroku.com/
        Installation of the Heroku CLI (Command Line Interface): https://devcenter.heroku.com/articles/heroku-cli
        Terminal Command:
            heroku login - Authenticate with Heroku.
            heroku create - Used to create a new application (Heroku app) on the Heroku platform.
            git push heroku main - Deploys the code to the Heroku app.
            heroku open - Displays the link to the remotely running project.

    After use, don't forget to delete or stop the project on the Heroku platform. 
    You can go to heroku.com, select the running project, go to settings, and then delete the project.
